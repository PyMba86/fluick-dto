<?php

namespace Fluick\Dto;

use ReflectionClass;
use ReflectionProperty;

/**
 * Represents property of data transfer object
 *
 * @package Fluick\Dto
 */
abstract class Property
{
    const SCALAR_INT = 'int';
    const SCALAR_INTEGER = 'integer';
    const SCALAR_BOOL = 'bool';
    const SCALAR_BOOLEAN = 'boolean';
    const SCALAR_FLOAT = 'float';
    const SCALAR_DOUBLE = 'double';

    const SCALAR_TYPES = [
        self::SCALAR_INT,
        self::SCALAR_INTEGER,
        self::SCALAR_BOOL,
        self::SCALAR_BOOLEAN,
        self::SCALAR_FLOAT,
        self::SCALAR_DOUBLE,
    ];

    const NORMALIZING_TYPE_MAP = [
        self::SCALAR_INT => 'integer',
        self::SCALAR_BOOL => 'boolean',
        self::SCALAR_FLOAT => 'float',
        self::SCALAR_DOUBLE => 'float',
    ];

    /**
     * @var string
     */
    protected $name;

    /**
     * Property types
     *
     * @var string[]
     */
    protected $types = [];

    /**
     * Property is nullable
     *
     * @var bool
     */
    protected $isNullable = false;

    /**
     * TRUE if the property was declared at compile-time, or FALSE if it was created at run-time.
     *
     * @var bool
     */
    protected $isDefault = true;

    /**
     * Property is mixed
     *
     * @var bool
     */
    protected $isMixed = false;

    /**
     * Type contains 'array'/'iterable' declaration
     *
     * @var bool
     */
    protected $isMixedArray = false;

    /**
     * Declared array types
     *
     * @var string[]
     */
    protected $arrayTypes = [];

    /**
     * @var ReflectionProperty
     */
    protected $reflectionProperty;

    /**
     * @var ReflectionClass
     */
    protected $declaringClass;

    /**
     * @var boolean
     */
    protected $hasDefaultValue;

    public function __construct(ReflectionProperty $property)
    {
        $this->name = $property->getName();

        $this->isDefault = $property->isDefault();

        $this->reflectionProperty = $property;

        $this->declaringClass = $property->getDeclaringClass();

        $this->hasDefaultValue = !is_null($this->declaringClass->getDefaultProperties()[$this->name] ?? null);
    }

    /**
     * Property name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Array of available types
     *
     * @return string[]
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * Nullable field
     *
     * @return bool
     */
    public function isNullable(): bool
    {
        return $this->isNullable;
    }

    /**
     * Check the field has been declared at compile time
     *
     * @return bool
     */
    public function isDefault(): bool
    {
        return $this->isDefault;
    }

    /**
     * Field marked as mixed
     *
     * @return bool
     */
    public function isMixed(): bool
    {
        return $this->isMixed;
    }

    /**
     * There are 'array' or 'iterable' types
     *
     * @return bool
     */
    public function isMixedArray(): bool
    {
        return $this->isMixedArray;
    }

    /**
     * Typed array types like 'string[]' etc.
     *
     * @return string[]
     */
    public function getArrayTypes(): array
    {
        return $this->arrayTypes;
    }

    /**
     * Check value can be assigned to the property
     *
     * @param mixed $value
     * @return bool
     */
    public function isValidType($value): bool
    {
        if (!$this->types) {
            return true;
        }

        if ($this->isMixed) {
            return true;
        }

        if (is_iterable($value) && $this->isMixedArray) {
            return true;
        }

        if ($this->isNullable && $value === null) {
            return true;
        }

        if (is_iterable($value)) {
            foreach ($this->arrayTypes as $type) {
                $isValid = $this->assertValidArrayTypes($type, $value);

                if ($isValid) {
                    return true;
                }
            }
        }

        foreach ($this->types as $type) {
            $isValidType = $this->assertValidType($type, $value);

            if ($isValidType) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check property has default value
     *
     * @return bool
     */
    public function hasDefaultValue(): bool
    {
        return $this->hasDefaultValue;
    }

    /**
     * Get original reflection property
     *
     * @return ReflectionProperty
     */
    public function getReflectionProperty(): ReflectionProperty
    {
        return $this->reflectionProperty;
    }

    /**
     * Get declaring class
     *
     * @return ReflectionClass
     */
    public function getDeclaringClass(): ReflectionClass
    {
        return $this->declaringClass;
    }

    /**
     * Check property type is compatible with value type
     *
     * @param string $type
     * @param mixed $value
     *
     * @return bool
     */
    protected function assertValidType(string $type, $value): bool
    {
        return $value instanceof $type || gettype($value) === $type;
    }

    /**
     * Check type is compatible with collection values type
     *
     * @param string $type
     * @param $collection
     *
     * @return bool
     */
    protected function assertValidArrayTypes(string $type, $collection): bool
    {
        foreach ($collection as $value) {
            if (!$this->assertValidType($type, $value)) {
                return false;
            }
        }

        return true;
    }
}