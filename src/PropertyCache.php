<?php

namespace Fluick\Dto;

/**
 * Cache for classes properties
 *
 * @package Fluick\Dto
 */
class PropertyCache
{
    private static $cache = [];

    /**
     * Get DTO property from cache (or put it if cache is empty)
     *
     * @param  string  $className
     * @param  callable  $callBack  - callback to remember values
     *
     * @return Property[]
     */
    public static function getClassProperties(string $className, callable $callBack): array
    {
        $result = self::$cache[$className] ?? [];

        if (!$result) {
            self::$cache[$className] = $callBack();
        }

        return self::$cache[$className];
    }
}