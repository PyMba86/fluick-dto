<?php

namespace Fluick\Dto;

use ReflectionClass;
use ReflectionProperty;

/**
 * Class DataTransferObject
 *
 * @package Fluick\Dto
 */
abstract class DataTransferObject
{

    /**
     * DataTransferObject constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {
        $properties = $this->getProperties();

        foreach ($properties as $propertyName => $property) {

            $propertyValueSet = isset($parameters[$propertyName]);

            if (!$propertyValueSet) {
                // source does not contain value but there's default one, so ve leave it unchanged
                if ($property->hasDefaultValue()) {
                    continue;
                }

                if (!$property->isNullable() && !$property->isDefault()) {
                    // no source value, not default and field is not nullable, throwing exception
                    throw DataTransferObjectError::uninitialized(
                        static::class,
                        $propertyName
                    );
                }
            }

            $value = $parameters[$propertyName] ?? $this->{$propertyName} ?? null;

            $value = PropertyValueFactory::castValueToPropertyType($property, $value);

            if (!$property->isValidType($value)) {
                throw DataTransferObjectError::invalidType(
                    static::class,
                    $propertyName,
                    $property->getTypes(),
                    $value
                );
            }

            $this->{$propertyName} = $value;
        }
    }

    /**
     * @return Property[]
     */
    private function getProperties(): array
    {
        // cache data transfer object properties
        return PropertyCache::getClassProperties(static::class, function () {

            $result = [];

            $class = new ReflectionClass(static::class);

            $properties = $class->getProperties(ReflectionProperty::IS_PUBLIC);

            foreach ($properties as $reflectionProperty) {

                if ($reflectionProperty->isStatic()) {
                    continue;
                }

                $property = new DocTypedProperty($reflectionProperty);

                $field = $property->getName();

                $result[$field] = $property;
            }

            return $result;
        });
    }
}