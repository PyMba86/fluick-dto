<?php

namespace Fluick\Dto;

class PropertyValueFactory
{
    const SCALAR_INT = 'int';
    const SCALAR_INTEGER = 'integer';
    const SCALAR_BOOL = 'bool';
    const SCALAR_BOOLEAN = 'boolean';
    const SCALAR_FLOAT = 'float';
    const SCALAR_DOUBLE = 'double';

    const SCALAR_TYPES = [
        self::SCALAR_INT,
        self::SCALAR_INTEGER,
        self::SCALAR_BOOL,
        self::SCALAR_BOOLEAN,
        self::SCALAR_FLOAT,
        self::SCALAR_DOUBLE,
    ];

    /**
     * Cast $value to declared property type if needed
     *
     * @param Property $property
     *
     * @param mixed $value
     * @return mixed
     */
    public static function castValueToPropertyType(Property $property, $value)
    {
        // if property is mixed, return value as it is
        if ($property->isMixed()) {
            return $value;
        }

        // nullable property with nullable value
        if ($value === null) {
            return null;
        }

        $propertyTypes = $property->getTypes();

        $valueType = gettype($value);

        // if value type is allowed, pass as it is
        if (in_array($valueType, $propertyTypes)) {
            return $value;
        }

        // typed iterable properties values should be cast to DTO or collection of DTO, or be returned as they are
        if (is_iterable($value)) {

            return $value;

        } elseif (is_scalar($value)) {
            // cast scalar value to closest allowed scalar type
            foreach ($propertyTypes as $type) {
                if (!in_array($type, self::SCALAR_TYPES)) {
                    continue;
                }

                settype($value, $type);

                break;
            }
        }

        return $value;
    }

    /**
     * Cast an array to DTO instance if possible. Otherwise same $value is returned
     *
     * @param mixed $value
     * @param array|iterable $types
     *
     * @return mixed
     */
    protected static function castToDataTransferObject($value, array $types)
    {
        // cast to another DTO instance
        foreach ($types as $type) {

            if (is_subclass_of($type, DataTransferObject::class)) {
                return new $type($value);
            }

            continue;
        }

        return $value;
    }

    /**
     * Cast multidimensional array to typed array is possible. Otherwise same array is being returned
     * If $arrayTypes contain DTO class, collection of DTO will return
     *
     * @param $values
     * @param array|iterable $arrayTypes
     *
     * @return DataTransferObject[]
     */
    protected static function castToDataTransferObjectCollection($values, array $arrayTypes): array
    {
        $castTo = null;

        foreach ($arrayTypes as $type) {
            if (!is_subclass_of($type, DataTransferObject::class)) {
                continue;
            }

            $castTo = $type;

            break;
        }

        if (!$castTo) {
            return $values;
        }

        $casts = [];

        foreach ($values as $value) {
            $casts[] = new $castTo($value);
        }

        return $casts;
    }

    /**
     * Check source should be cast to collection
     *
     * @param array|iterable $values
     * @return bool
     */
    protected static function shouldBeCastToCollection($values): bool
    {
        if (empty($values)) {
            return false;
        }

        foreach ($values as $key => $value) {
            // associative array should not to be casted to collection
            if (is_string($key)) {
                return false;
            }

            // plain arrays should not to be casted to collection
            if (!is_array($value)) {
                return false;
            }
        }

        return true;
    }
}