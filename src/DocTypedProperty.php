<?php

namespace Fluick\Dto;

use ReflectionClass;
use ReflectionNamedType;
use ReflectionProperty;

/**
 * Realization of Property class where types are being resolved through `@var` annotations
 *
 * @package Fluick\Dto
 */
class DocTypedProperty extends Property
{
    const VAR_DOCBLOCK_REGEX = '/@var ((?:(?:[\w?|\\\\<>])+(?:\[])?)+)/';

    const USE_DOCBLOCK_REGEX = '/@uses ((?:(?:[\w?|\\\\<>])+(?:\[])?)+)/';

    /**
     * Type declaration as string
     *
     * @var string|null
     */
    protected $typeDeclaration;

    /**
     * Use declaration as string
     *
     * @var array
     */
    protected $useDeclaration;

    public function __construct(ReflectionProperty $property)
    {
        parent::__construct($property);

        $this->useDeclaration = $this->extractUse($property);

        $this->types = $this->extractTypes($property);

        $this->typeDeclaration = implode('|', $this->types);

        $this->isNullable = $this->checkIsNullable($this->types);

        $this->isMixed = $this->checkIsMixed($this->types);

        $this->isMixedArray = $this->checkIsMixedArray($this->types);

        $this->arrayTypes = $this->resolveArrayTypes($this->types);
    }

    /**
     * Get array of property use
     *
     * @param ReflectionProperty $property
     * @return array
     */
    private function extractUse(ReflectionProperty $property): array
    {
        // extract types from doc comment
        $docComment = $property->getDocComment();

        preg_match_all(
            self::USE_DOCBLOCK_REGEX,
            $docComment,
            $useDeclaration
        );

        return $useDeclaration[1];
    }

    /**
     * Get array of property types
     *
     * @param ReflectionProperty $property
     * @return array
     */
    private function extractTypes(ReflectionProperty $property): array
    {
        // extract types from doc comment
        $docComment = $property->getDocComment();

        preg_match(
            self::VAR_DOCBLOCK_REGEX,
            $docComment,
            $varStrMatches
        );

        $typeDeclaration = trim($varStrMatches[1] ?? '');

        // since PHP 7.4 we should take into account declared type
        if (method_exists($property, 'getType') && $reflectionType = $property->getType()) {

            if ($reflectionType->allowsNull()) {
                $typeDeclaration .= '|null';
            }

            if ($reflectionType instanceof ReflectionNamedType) {
                $typeDeclaration .= '|' . $reflectionType->getName();
            }
        }

        $declaration = array_merge(
            explode('|', $typeDeclaration), $this->useDeclaration);

        return $this->normalizeTypes($declaration, $property->getDeclaringClass());
    }

    /**
     * Replace scalar declarations with full variants
     * Replace short class names with full class names
     *
     * @param array $types - type declaration
     * @param ReflectionClass $rs - class where property declared
     * @return array
     * @see DocTypedProperty::NORMALIZING_TYPE_MAP - type map
     */
    private function normalizeTypes(array $types, ReflectionClass $rs): array
    {
        $types = array_filter(array_map(
            function (string $type) use ($rs) {
                $type = self::NORMALIZING_TYPE_MAP[$type] ?? $type;

                // return empty type, scalar type immediately
                if (!$type || in_array($type, self::SCALAR_TYPES, true)) {
                    return $type;
                }

                // replace short class name with full one
                $splitRegex = '/([\[|]|<|>|])/i';
                $type = preg_split($splitRegex, $type, -1, PREG_SPLIT_DELIM_CAPTURE);

                return implode(array_map(function ($type) use ($splitRegex) {
                    if (!$type) {
                        return $type;
                    }

                    // split symbols and php doc types should not be cast
                    if (preg_match($splitRegex, $type)) {
                        return $type;
                    }

                    return $type;

                }, $type));
            },
            $types
        ));

        return $types;
    }

    /**
     * Define whether the property is nullable
     *
     * @param string[] $types
     * @return bool
     */
    private function checkIsNullable(array $types): bool
    {
        foreach ($types as $type) {
            if (in_array($type, ['mixed', 'null', '?'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check type declared `mixed`
     *
     * @param string[] $types
     * @return bool
     */
    private function checkIsMixed(array $types): bool
    {
        return !$types || in_array('mixed', $types);
    }

    /**
     * Check property contains some of ['array', 'iterable'] declarations
     *
     * @param string[] $types
     * @return bool
     */
    private function checkIsMixedArray(array $types): bool
    {
        foreach ($types as $type) {
            if (in_array($type, ['array', 'iterable', 'mixed[]', 'iterator'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get typed array declaration
     *
     * @param string[] $types
     * @return array
     */
    private function resolveArrayTypes(array $types): array
    {
        $iterableTypes = array_filter(
            array_map(function ($type) {
                if (strpos($type, '[]') !== false) {
                    return str_replace('[]', '', $type);
                }

                if (strpos($type, 'iterable<') !== false) {
                    return str_replace(['iterable<', '>'], ['', ''], $type);
                }

                if (strpos($type, 'iterator<') !== false) {
                    return str_replace(['iterator<', '>'], ['', ''], $type);
                }

                return null;
            }, $types)
        );

        return array_merge($iterableTypes, $this->useDeclaration);
    }
}