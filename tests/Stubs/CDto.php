<?php

namespace Fluick\Dto\Tests\Stubs;

use ArrayIterator;
use Fluick\Dto\DataTransferObject;

class CDto extends DataTransferObject
{
    /**
     * @var boolean|ArrayIterator
     */
    public $booleanField;
}