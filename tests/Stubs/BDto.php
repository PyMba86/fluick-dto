<?php

namespace Fluick\Dto\Tests\Stubs;

use Fluick\Dto\DataTransferObject;

/**
 * Class BDto
 * @package Fluick\Dto\Tests\Stubs
 */
class BDto extends DataTransferObject
{
    /**
     * @var integer
     */
    public static $staticField;

    /**
     * @var integer
     */
    public $integerField;

    /**
     * @var null|CDto
     * @uses \Fluick\Dto\Tests\Stubs\CDto
     */
    public $dtoField;
}