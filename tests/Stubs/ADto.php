<?php

namespace Fluick\Dto\Tests\Stubs;

use DateTime;
use Fluick\Dto\DataTransferObject;

class ADto extends DataTransferObject
{
    /**
     * @var DateTime
     */
    public $dateField;

    /**
     * @var BDto
     * @uses \Fluick\Dto\Tests\Stubs\BDto
     */
    public $bField;

    /**
     * @var BDto[]
     * @uses \Fluick\Dto\Tests\Stubs\BDto
     */
    public $bArrayField;
}