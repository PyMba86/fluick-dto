<?php

namespace Fluick\Dto\Tests;

use ArrayIterator;
use Fluick\Dto\DataTransferObject;
use Fluick\Dto\DataTransferObjectError;
use Fluick\Dto\Tests\Stubs\ADto;
use Fluick\Dto\Tests\Stubs\BDto;
use Fluick\Dto\Tests\Stubs\CDto;
use PHPUnit\Framework\TestCase;
use stdClass;

class DataTransferObjectTest extends TestCase
{
    function testResolveAnnotatedParamSupport()
    {
        $v = 999;
        $dto = new class(['field' => $v]) extends DataTransferObject {
            /** @param int */
            public $field;
        };

        $this->assertSame($v, $dto->field);
    }

    function testResolveNotAnnotatedSource()
    {
        $v = 999;

        $dto = new class(['field' => $v]) extends DataTransferObject {
            public $field;
        };

        $this->assertSame($v, $dto->field);
    }

    function testUnionTypes()
    {
        $v = 999;

        $dto = new class(['fieldName' => $v]) extends DataTransferObject {
            /** @var string|integer */
            public $fieldName;
        };

        $this->assertSame($v, $dto->fieldName);

        $v = 1;

        $dto = new class(['fieldName' => $v]) extends DataTransferObject {
            /** @var string|boolean|integer */
            public $fieldName;
        };

        $this->assertSame($v, $dto->fieldName);
    }

    public function testMixedProperty()
    {
        $dto = new class(['fieldName' => 'string']) extends DataTransferObject {
            /** @var mixed|boolean */
            public $fieldName;
        };

        $this->assertSame('string', $dto->fieldName);

        $dto = new class(['fieldName' => 'string']) extends DataTransferObject {
            /** @var null|boolean|mixed| */
            public $fieldName;
        };

        $this->assertSame('string', $dto->fieldName);
    }

    function testNullablePropertyFromNotExistingSource()
    {
        $dto = new class(['existingField' => 999]) extends DataTransferObject {
            /**
             * @var null|integer
             */
            public $fieldName;
        };

        $this->assertSame(null, $dto->fieldName);
    }

    function testArrayTypedProperty()
    {
        $v = [1, 2, 3];

        $dto = new class(['fieldName' => $v]) extends DataTransferObject {
            /** @var array */
            public $fieldName;
        };

        $this->assertSame($v, $dto->fieldName);

        $dto = new class(['fieldName' => $v]) extends DataTransferObject {
            /** @var integer[] */
            public $fieldName;
        };

        $this->assertSame($v, $dto->fieldName);

        $this->expectException(DataTransferObjectError::class);

        $dto = new class(['fieldName' => $v]) extends DataTransferObject {
            /**
             * @var string[]
             */
            public $fieldName;
        };
    }

    function testNotDtoCastTypeFromArray()
    {
        $this->expectException(DataTransferObjectError::class);

        $dto = new class(['fieldName' => [['field' => 1], ['field' => 2]]]) extends DataTransferObject {
            /** @var ArrayIterator */
            public $fieldName;
        };
    }

    function testCastEmptyArray()
    {
        $dto = new class(['fieldName' => []]) extends DataTransferObject {
            /** @var integer[] */
            public $fieldName;
        };

        $this->assertEquals([], $dto->fieldName);
    }

    function testAssociativeArrayTypedProperty()
    {
        $v = [
            'field1' => 1,
            'field2' => 2,
        ];

        $dto = new class(['fieldName' => $v]) extends DataTransferObject {
            /** @var array */
            public $fieldName;
        };

        $this->assertSame($v, $dto->fieldName);
    }

    function testNestedDto()
    {
        $c1Dto = new CDto([
            'booleanField' => true
        ]);

        $b1Dto = new BDto([
            'integerField' => 123,
        ]);

        $b2Dto = new BDto([
            'integerField' => 123,
            'dtoField' => $c1Dto
        ]);

        $dto = new ADto([
            'dateField' => date_create(),
            'bField' => $b1Dto,
            'bArrayField' => [$b1Dto, $b2Dto]
        ]);

        $this->assertNull($dto->bArrayField[0]->dtoField);
        $this->assertTrue($dto->bArrayField[1]->dtoField->booleanField);

        $b1sDto = new BDto([
            'integerField' => '999'
        ]);

        $dto = new class([
            'bField' => $b1sDto,
        ]) extends DataTransferObject {
            /**
             * @var BDto
             * @uses \Fluick\Dto\Tests\Stubs\BDto
             */
            public $bField;
        };

        $this->assertSame($dto->bField->integerField, 999);

        $b2sDto = new BDto([
            'integerField' => '888'
        ]);

        $dto = new class([
            'bFieldArr' => [$b1sDto, $b2sDto],
        ]) extends DataTransferObject {
            /**
             * @var BDto[]
             * @uses \Fluick\Dto\Tests\Stubs\BDto
             */
            public $bFieldArr;
        };

        $this->assertSame($dto->bFieldArr[0]->integerField, 999);

        $this->assertSame($dto->bFieldArr[1]->integerField, 888);
    }

    public function testIterableSupport()
    {
        $v = [1, 2, 3];

        $dto = new class(['fieldName' => new ArrayIterator($v)]) extends DataTransferObject {
            /** @var iterable */
            public $fieldName;
        };

        $this->assertEquals(new ArrayIterator($v), $dto->fieldName);

        $dto = new class(['fieldName' => new ArrayIterator($v)]) extends DataTransferObject {
            /** @var iterable<integer> */
            public $fieldName;
        };

        $this->assertEquals(new ArrayIterator($v), $dto->fieldName);

    }

    public function testStaticFieldsAreNotFilled()
    {

        $dto = new class(['staticField' => true]) extends DataTransferObject {
            public static $staticField;
        };

        $this->assertNull($dto::$staticField);
    }

    public function testUninitializedError()
    {
        $dto = new class([]) extends DataTransferObject {
            /** @var integer */
            public $integerField = 123;
        };

        $this->assertEquals(123, $dto->integerField);
    }

    public function testNotScalarAndNotDtoType()
    {
        $this->expectException(DataTransferObjectError::class);

        $dto = new class(['field' => 1]) extends DataTransferObject {
            /** @var stdClass */
            public $field;
        };
    }

    public function testIntegerCasts()
    {
        $dto = new class([
            'f' => 'string'
        ]) extends DataTransferObject {
            /** @var integer */
            public $f;
        };
        $this->assertEquals(0, $dto->f);

        $dto = new class([
            'f' => true
        ]) extends DataTransferObject {
            /** @var integer */
            public $f;
        };
        $this->assertEquals(1, $dto->f);


        $this->expectException(DataTransferObjectError::class);
        $dto = new class([
            'f' => []
        ]) extends DataTransferObject {
            /** @var integer */
            public $f;
        };
        $this->assertEquals(0, $dto->f);
    }
}